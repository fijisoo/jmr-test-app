import React, { Component } from 'react';
import {Landing} from "./pages/Landing";
import './App.scss';
import {BrowserRouter as Router, Route} from "react-router-dom";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
            <Route exact path="/" component={Landing} />
        </Router>
      </div>
    );
  }
}

export default App;
