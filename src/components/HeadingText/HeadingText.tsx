import React from 'react';
import './HeadingText.scss';

export const HeadingText = () => {
    return (
        <span className="HeadingText">
            Raspberry kingdom
        </span>
    )
};