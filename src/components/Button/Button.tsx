import React from 'react';
import {ButtonProps} from "./Button.types";
import './Button.scss';

export const Button = ({text, onClick, type = 'button', disabled, className}: ButtonProps) => {
    return (
        <button
            className={`button ${type == 'button' ? 'button--default' : 'button--submit'} ${className && className}`}
            onClick={() => onClick && onClick()} type={type && type} disabled={disabled && disabled}>
            {text}
        </button>
    )
};