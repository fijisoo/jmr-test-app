type ButtonTypes = 'submit' | 'button';

export interface ButtonProps {
    text: string,
    onClick?: () => void,
    type?: ButtonTypes,
    disabled?: boolean,
    className?: string
}