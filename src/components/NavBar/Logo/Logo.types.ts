export interface LogoProps {
    customClassName?: string;
    onClick?: () => void;
}