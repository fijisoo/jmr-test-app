import React from 'react';
import { ReactComponent as SvgLogo } from '../../../assets/raspberry.svg';
import {LogoProps} from "./Logo.types";
import './Logo.scss';

export const Logo = ({customClassName, onClick}: LogoProps) => <SvgLogo className={customClassName ? `${customClassName} logo` : 'logo'} onClick={() => onClick && onClick()}/>;