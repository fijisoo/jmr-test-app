import React, {PureComponent} from 'react';
import { Link } from 'react-router-dom'
import { Logo } from "./Logo";
import './NavBar.scss';

export class NavBar extends PureComponent <{},{isToggled: boolean}>{

    state = {
        isToggled: false,
    };

    onToggle = () => this.setState({isToggled: !this.state.isToggled});

    render(){
        return (
            <header className="JMR__header">
                <ul className={`JMR__header-list ${this.state.isToggled ? 'JMR__header-list--collapsed' : ''}`}>
                    <li className="JMR__header-list-item"><Link to="">ABOUT</Link></li>
                    <li className="JMR__header-list-item"><Link to="">OFFER</Link></li>
                </ul>
                <Logo customClassName="JMR__header-logo--desktop"/>
                <Logo customClassName="JMR__header-logo--mobile" onClick={this.onToggle}/>
                <ul className={`JMR__header-list ${this.state.isToggled ? 'JMR__header-list--collapsed' : ''}`}>
                    <li className="JMR__header-list-item"><Link to="">GALLERY</Link></li>
                    <li className="JMR__header-list-item"><Link to="">CONTACT</Link></li>
                </ul>
            </header>
        )
    }
}