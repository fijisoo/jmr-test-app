import React from 'react';
import './Error.scss';

export const Error = ({text}: {text: string}) => {
    return (
        <span className="error-message">{text}</span>
    )
};