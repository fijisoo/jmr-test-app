import React, {PureComponent} from "react";
import {InputProps, InputState} from "./Input.types";
import './Input.scss';

export class Input extends PureComponent<InputProps, InputState>{

    static defaultProps = {
        placeholder: '',
        value: '',
    };

    static getDerivedStateFromProps(nextProps: Partial<InputProps>, prevState: Partial<InputState>) {
        if (prevState.propsValue !== nextProps.value) {
            return {
                value: nextProps.value,
                propsValue: nextProps.value,
            };
        }
        return null;
    }


    state = {
        value: '',
        propsValue: '',
    };


    handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({value: event.currentTarget.value}, () => {
            if(this.props.onChange){
                this.props.onChange({ name: this.props.name, value: this.state.value });
            }
        })
    };

    handleBlur = (event: React.FormEvent<HTMLInputElement>) => {
        const value = event.currentTarget.value;
        if (this.props.onBlur) {
            this.props.onBlur({ name: this.props.name, value });
        }
    };

    render(){
        const { type, placeholder } = this.props;
        return(
            <input value={this.state.value} onChange={this.handleChange} onBlur={this.handleBlur} type={type && type} className="input" placeholder={placeholder && placeholder} />
        )
    }
}