type InputTypes = 'email' | 'password' | 'text';

export interface InputProps {
    name: string;
    placeholder?: string;
    type?: InputTypes;
    onChange?: (data: { name: string; value: any }) => void;
    onBlur?: (data: { name: string; value: any }) => void;
    value?: string;
}

export interface InputState {
    value: string | number;
    propsValue: string | number;
}