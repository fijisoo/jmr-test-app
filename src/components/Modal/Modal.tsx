import React, {PureComponent} from 'react';
import * as ReactModal from 'react-modal';
import {Input} from "../Input";
import {Button} from "../Button";
import {ModalPropsWithFormik} from "./Modal.types";
import './Modal.scss';
import {Error} from "../Error";

const ModalComponent = ReactModal.default;

export class Modal extends PureComponent <ModalPropsWithFormik> {

    handleChange = (data: { name: string; value: any }) => {
        const { setFieldValue, setFieldTouched, setStatus } = this.props;
        setFieldValue(data.name, data.value);
        setFieldTouched(data.name, true);
        setStatus(undefined);
    };

    handleBlur = (data: { name: string; value: any }) => {
        const { setFieldValue, setFieldTouched } = this.props;
        setFieldValue(data.name, data.value);
        setFieldTouched(data.name, true);
    };

    render() {
        const { handleSubmit, values, errors, touched, isSubmitting, status } = this.props;
        return (
            <ModalComponent
                isOpen={this.props.isOpened}
                className="modal"
                contentLabel="onRequestClose Example"
                onRequestClose={() => this.props.onCloseModal()}
                onAfterClose={() => this.props.resetForm({email: '', password: ''})}
                style={{
                    overlay: {
                        backgroundColor: 'rgba(0,0,0,0.7)'
                    }
                }}
            >
                <span className="modal__title">Are you a Raspberry Knight?</span>
                        <form onSubmit={handleSubmit} className="modal__form">
                            <Input name="email" placeholder="Email" type="email" onChange={this.handleChange} onBlur={this.handleBlur} value={values.email}/>
                            {errors.email && touched.email && <Error text={errors.email}/>}
                            <Input name="password" placeholder="Password" type="password" onChange={this.handleChange} onBlur={this.handleBlur} value={values.password}/>
                            {errors.password && touched.password && <Error text={errors.password}/>}
                            {status && status.server && <Error text={status.server}/>}
                            <Button text="LOG IN" type="submit" disabled={isSubmitting} className="modal__form-button"/>
                        </form>
            </ModalComponent>
        );
    }
}

ReactModal.default.setAppElement('#root');