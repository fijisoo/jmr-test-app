import {FormikProps} from "formik";

export interface ModalProps {
    isOpened: boolean,
    onCloseModal: () => void
}

export interface ModalPropsWithFormik extends FormikProps<{ email: string, password: string }>{
    isOpened: boolean,
    onCloseModal: () => void
}