import React from 'react';
import './Footer.scss';
import {Link} from "react-router-dom";

export const Footer = () => {
    return (
        <footer className="JMR__footer-wrapper">
            <div className="JMR__footer">
                <span className="JMR__footer-item">© 2014 RASPBERRY KINGDOM</span>
                <div className="JMR__footer-item--center">
                    <Link className="JMR__footer-item-link" to="">Cookies</Link> <span className="JMR__footer-pipe"/>
                    <Link className="JMR__footer-item-link" to="">Privacy</Link>
                </div>
                <span className="JMR__footer-item">Design by<a className="JMR__footer-item-link--bold" href=""> Wizard of Oz</a></span>
            </div>
        </footer>
    )
};