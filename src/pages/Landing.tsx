import React, {PureComponent} from "react";
import {NavBar} from "../components/NavBar";
import {HeadingText} from "../components/HeadingText";
import {Footer} from "../components/Footer";
import {Button} from "../components/Button";
import {Modal} from "../components/Modal";
import {withFormik} from "formik";
import './Landing.scss';
import {ModalProps} from "../components/Modal/Modal.types";
import axios from 'axios';

const ModalForm = withFormik<ModalProps, { email: string, password: string }>({
        validate:
            values => {
                let errors: any = {};
                if (!values.email) {
                    errors.email = 'Required';
                } else if (
                    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                ) {
                    errors.email = 'Invalid email address';
                }
                return errors;
            },
        handleSubmit: (values, bag) => {
            // axios.interceptors.request.use(request => {
            //     console.log('Starting Request', request)
            //     return request
            // });
            axios.post('https://recruitment-api.pyt1.stg.jmr.pl/login', {
                login: values.email,
                password: values.password
            }, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            }).then(
                (response) => {
                    bag.setStatus({server: response.data.message});
                    bag.setSubmitting(false);
                },
                (rejected) => {
                    bag.setStatus({server: rejected.data.message});
                    bag.setSubmitting(false);
                }
            )
        },
        displayName: 'Modal Form',
    }
)(Modal);

export class Landing extends PureComponent <{}, { showModal: boolean }> {

    state = {
        showModal: false,
    };

    handleOpenModal = () => this.setState({showModal: true});

    handleCloseModal = () => this.setState({showModal: false});

    render() {
        return (
            <>
                <NavBar/>
                <div className="landing__content">
                    <HeadingText/>
                    <Button text="ENTER THE GATES" onClick={this.handleOpenModal}/>
                </div>
                <Footer/>
                <ModalForm isOpened={this.state.showModal} onCloseModal={this.handleCloseModal}/>
            </>
        )
    }
}