This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Test app for JRM company

App created with React, Typescript, Webpack, SCSS, react-modal, formik, axios.

U can run it on local device with installed Node and Node Package Menager. In console run: 
1. > npm i
2. > npm start
3. open localhost:3000 in your web browser
